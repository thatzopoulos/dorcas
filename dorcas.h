/* 
** File: dorcas.h
**
** Purpose:
** Header File for the terminal text editor Dorcas
**
** References:
** [1] Nasa Flight Software C Coding Standard Documentation On Header Files
** 
** Notes:
** In the middle of moving stuff to this header, attempting to follow the Nasa C Coding Standard for Headers
 */

#ifndef _dorcas_
#define _dorcas_

/*
** Includes
*/

#include <time.h>
#include <termios.h>

/*
** Macro Definitions
*/

#define DORCAS_TAB_STOP 8
#define DORCAS_VERSION "0.1.0"
#define DORCAS_QUIT_TIMES 1
#define CTRL_KEY(k) ((k)&0x1f)

/*
** Type Definitions
*/

/*** data ***/

struct editorSyntax
{
    char *filetype;
    char **filematch;
    char **keywords;
    char *singleline_comment_start;
    char *multiline_comment_start;
    char *multiline_comment_end;
    int flags;
};

typedef struct erow
{
    int idx;
    int size;
    int rsize;
    char *chars;
    char *render;
    unsigned char *hl;
    int hl_open_comment;
} erow;

struct editorConfig
{
    int cx, cy; // cursors x and y position (index into the chars field of an erow)
    int rx;     // index into the render field of an erow (if there are no tabs, it will be the same as E.cx. If there are tabs, it wil be larger by the num of extra spaces those tabs take up when rendered)
    int rowoffset;
    int coloffset;
    int screenrows;
    int screencols;
    int numrows;
    erow *row;
    int dirty;
    char *filename;
    char statusmsg[80];
    struct editorSyntax *syntax;
    time_t statusmsg_time;
    struct termios orig_termios;
};

struct editorConfig E;

/*** filetypes ***/
char *C_HL_extensions[] = {".c", ".h", ".cpp", NULL};
char *C_HL_keywords[] = {
    "switch", "if", "while", "for", "break", "continue", "return", "else",
    "struct", "union", "typedef", "static", "enum", "class", "case",
    "int|", "long|", "double|", "float|", "char|", "unsigned|", "signed|",
    "void|", NULL};

char *LAIN_HL_extensions[] = {".lain", NULL};
char *LAIN_HL_keywords[] = {"else", "fn", "if", "let", "return", "len", "puts", "push", "rest", "first", "last", "int", NULL};

#define HL_HIGHLIGHT_NUMBERS (1 << 0)
#define HL_HIGHLIGHT_STRINGS (1 << 1)

struct editorSyntax HLDB[] = {
    {"c",
     C_HL_extensions,
     C_HL_keywords,
     "//", "/*", "*/",
     HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS},
    {"lain",
     LAIN_HL_extensions,
     LAIN_HL_keywords,
     "//", "/*", "*/",
     HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS},
};

#define HLDB_ENTRIES (sizeof(HLDB) / sizeof(HLDB[0]))

// 1000 is used because its large enough not to effect any of the values in the range of a char input from the keyboard
enum editorKey
{
    BACKSPACE = 127,
    ARROW_LEFT = 1000,
    ARROW_RIGHT,
    ARROW_UP,
    ARROW_DOWN,
    DEL_KEY,  // <esc>[3~
    HOME_KEY, // <esc>[1~, <esc>[7~, <esc>[H, or <esc>OH
    END_KEY,  // <esc>[4~, <esc>[8~, <esc>[F, or <esc>OF
    PAGE_UP,  // <esc>[5~
    PAGE_DOWN //<esc>[6~

};

enum editorHighlight
{
    HL_NORMAL = 0,
    HL_COMMENT,
    HL_MLCOMMENT,
    HL_KEYWORD1,
    HL_KEYWORD2,
    HL_STRING,
    HL_NUMBER,
    HL_MATCH
};

struct append_buffer
{
    char *b; // pointer of buffer in memory
    int len;
};

// represents an empty busffer, is a constructor for the append_buffer type
#define APPEND_BUFFER_INIT \
    {                      \
        NULL, 0            \
    }

/*
** Exported Functions
*/

#endif /* _dorcas_ */
