# Source, Executable, Includes, Library Defines
INCL   = dorcas.h
SRC    = dorcas.c
OBJ    = $(SRC:.c=.o)
LIBS   = -lgen
EXE    = dorcas

# Compiler, Linker Defines
CC      = /usr/bin/gcc
CFLAGS  = -Wall -Wextra -pedantic -std=c99

# Compile and Assemble C Source File
dorcas: dorcas.c
	$(CC) $(SRC) -o $(EXE) $(CFLAGS)

.PHONY: clean

clean:
	rm dorcas