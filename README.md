
# Dorcas


`Dorcas` is a simple terminal text editor written in C. Like Kilo, Dorcas does not depend on any library including ncurses. It uses terminal escape sequences, primarily those of the VT100.


Dorcas is based on the [kilo text editor](https://viewsourcecode.org/snaptoken/kilo/)

## Usage

``` dorcas <filename> ```

## Controls
    CTRL-S: Save
    CTRL-Q: Quit
    CTRL-F: Find string in file (ESC to exit search, arrows to navigate through results)

## Additional Features
- Syntax highlighting for C/C++/LAIN

## Random Links

- VT100 User Guide Escape Sequences : https://vt100.net/docs/vt100-ug/chapter3.html
- Ideas for additional features : https://viewsourcecode.org/snaptoken/kilo/08.appendices.html#ideas-for-features-to-add-on-your-own
- bugfixes to check out https://github.com/antirez/kilo/pull/78